@dynamic = undefined

@static = undefined

@chests = undefined

@item =
  delete: (opt) ->
    if opt.obj includes opt.item.ne
      delete opt.obj[opt.item.ne]
      console.log "#{opt.item.ne} Deleted."
    else
      console.log "#{opt.item.ne} does not exist in #{opt.obj}."
  add: (opt) ->
    if opt.obj includes opt.item.ne
      opt.obj[opt.item.ne] = opt.item.nu
      console.log "#{opt.item.ne} Added."
    else
      console.log "#{opt.item.ne} does not exist in #{opt.obj}."
  plus: (opt) ->
    if opt.obj includes opt.item.ne and opt.item.ne.typeof is String
      opt.obj[opt.item.ne] += opt.item.nu
      console.log "#{opt.item.ne} was incremented by #{opt.item.nu}."
    else if opt.obj includes opt.item.ne and opt.item.ne.typeof isnt String
      console.log "#{opt.item.ne} isn't a string."
    else
      console.log "#{opt.item.ne} does not exist in #{opt.obj}."
  minus: (opt) ->
    if opt.obj includes opt.item.ne and opt.item.ne.typeof is String
      opt.obj[opt.item.ne] -= opt.item.nu
      console.log "#{opt.item.ne} was incremented by #{opt.item.nu}."
    else if opt.obj includes opt.item.ne and opt.item.ne.typeof isnt String
      console.log "#{opt.item.ne} isn't a string."
    else
      console.log "#{opt.item.ne} does not exist in #{opt.obj}."
