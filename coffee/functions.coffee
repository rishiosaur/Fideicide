###
These are the functions to be used in the development of the game.
These are not the ones that the gamer uses.
To see those, go to commands.coffee or commands.js.
To work on commands.js, read the development rules.
###

# Commands to manipulate HTML
@html =
  # The input variable, it's local.
  input: document.getElementById 'input'

  # The function that prints a line. Pretty self-explanatory.
  print: (string) ->
    p = document.getElementById 'cmd'
    e = document.createElement 'p'
    text = document.createTextNode string
    e.appendChild text
    p.appendChild e
    misc.current.user.tEnter = false
    console.log "Printed #{string} to the game console."
  # The function that sends a print statement and recieves an answer.
  question: (string, returnstring) ->
    if misc.current.user.tEnter is false
      html.print(string)
      while misc.current.user.tEnter is false
        if misc.current.user.tEnter
          html.print returnstring
        else
          misc.current.user.tEnter = false
          html.print string

  # Gets the input value
  io: (event) ->
    x = event.keyCode
    p = document.getElementById 'cmd'
    br = document.createElement 'br'
    text = document.createTextNode input.value
    e = document.createElement 'p'
    if x is 13
      e.appendChild text
      p.appendChild e
      misc.current.user.text = text
      misc.current.user.tEnter = true
      console.log misc.current.user.text
      console.log misc.current.user.tEnter

#Commands that interact with the environment.
@world =
  LoadData: (place) ->

#The commands that interacts with the player object.
@playerfuncs =
  ###
  The command that makes the user go to a place.
  @param {string} place
  The place for the player to go to.
  ###
  goTo: (place) ->
    if places.includes place
      player.location.name = place
      console.log "Location updated to #{place}"
      world.LoadData(place)
    else
      console.error "Location #{place} does not exist"
  ###
  @param {Object} options
  The options that the equip function uses.
  It should include the item and its type.
  ###
  equip: (item) ->
    switch item.type.top
      when weapon
        if player.inventory.weapons.includes item.name
          player.weapon = player.inventory.weapons[item.name]
        else
          console.error "#{item.name} does not exist in #{item.type.sub}."
      when food
        console.log "Switched to food"
      when med
        console.log "Switched to med"
      when misc
        console.log "Switched to misc"
    #End switch statement
